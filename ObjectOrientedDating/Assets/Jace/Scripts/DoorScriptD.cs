﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorScriptD : MonoBehaviour
{

    public string Designation;

    public GameObject OpenPanel = null;

    void Start()
    {
        GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.save();
    }

    // Update is called once per frame
    public void OnTriggerEnter(Collider collision)
    {
        Debug.Log("Trigger entered");
        if (collision.gameObject.tag == "Player")
        {
            //Debug.Log("Player collided with door with designation: " + Designation);
            OpenPanel.SetActive(true);
        }

    }
    

    public void OnTriggerStay(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            // Debug.Log("Colliding!!");
            if (Input.GetMouseButton(0))
            {
                // Debug.Log("Clicked in Stay");
                int scene = GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.scene;

                switch (Designation)
                {
                    case "ryn":
                        GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.person = "ryn";
                        if (scene == 10 && !GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.items.Contains("cactus"))
                        {
                            GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.puzzleLevel = "puzzle3";
                            Save();
                            SceneManager.LoadScene("TestDialog");
                        }
                        else if (scene == 10 && GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.items.Contains("cactus"))
                        {
                            //nothing
                        }
                        else
                        {
                            Save();
                            SceneManager.LoadScene("TestDialog");
                        }
                        break;
                    case "jace":
                        GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.person = "jace";
                        if (scene == 12 && !GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.items.Contains("touchOfGray"))
                        {
                            GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.puzzleLevel = "puzzle5";
                            Save();
                            SceneManager.LoadScene("TestDialog");
                        }
                        else if (scene == 12 && GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.items.Contains("touchOfGray"))
                        {
                            //nothing
                        }
                        else if (scene == 9)
                        {
                            // also nothing?
                        }
                        else
                        {
                            Save();
                            SceneManager.LoadScene("TestDialog");
                        }
                        break;
                    case "seth":
                        GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.person = "seth";
                        if (scene == 4 && !GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.items.Contains("graphicsCard"))
                        {
                            GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.puzzleLevel = "puzzle4";
                            Save();
                            SceneManager.LoadScene("TestDialog");
                        }
                        else if (scene == 4 && GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.items.Contains("graphicsCard"))
                        {
                            //nothing
                        }
                        else
                        {
                            Save();
                            SceneManager.LoadScene("TestDialog");
                        }
                        break;
                    case "danny":
                        GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.person = "danny";
                        if (scene == 6 && !GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.items.Contains("gourmetCoffee"))
                        {
                            GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.puzzleLevel = "puzzle9";
                            Save();
                            SceneManager.LoadScene("TestDialog");
                        }
                        else if (scene == 16 && GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.items.Contains("gourmetCoffee"))
                        {
                            //nothing
                        }
                        else
                        {
                            Save();
                            SceneManager.LoadScene("TestDialog");
                        }
                        break;
                    case "night":
                        if (scene % 3 == 0)
                        {
                            GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.person = "night";
                            GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.scene++;
                            if (scene == 3)
                            {
                                GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.puzzleLevel = "puzzle2";
                                SceneManager.LoadScene("PuzzleTest");
                            }
                            Save();
                            SceneManager.LoadScene("TestDialog");
                        }
                        break;
                    case "closet":
                        if (scene < 11 && !GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.items.Contains("touchController"))
                        {
                            GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.puzzleLevel = "puzzle1";
                            Save();
                            SceneManager.LoadScene("PuzzleTest");
                        }
                        break;
                    case "Exit":
                        if (scene % 3 == 1)
                        {
                            GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.scene++;
                            GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.location = "oliver";
                            if (GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.scene == 17)
                            {
                                GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.person = "end";
                                Save();
                                SceneManager.LoadScene("TestDialog");
                                break;
                            }
                            Save();
                            SceneManager.LoadScene("Oliver");
                        }
                        break;
                }
            }
        }
    }

    public void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            OpenPanel.SetActive(false);
        }
    }

    void Save()
    {
        // GameObject.FindGameObjectWithTag("Data").GetComponent<DataScript>().data.save();
    }
}
