﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour {

    GameObject[] pauseObjects;
    GameObject player;
    // public GameObject controls;
    GameObject quitValidation;
    GameObject hintsPanel;

    // Use this for initialization
    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1;
        pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
        quitValidation = GameObject.Find("Quit Validation");
        hintsPanel = GameObject.Find("Hints Popup");
        hintsPanel.SetActive(false);
        quitValidation.SetActive(false);
        hidePaused();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {

        //uses the p button to pause and unpause the game
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseControl();
        }
    }


    //controls the pausing of the scene
    public void pauseControl()
    {
        if (Time.timeScale == 1)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0;
            player.GetComponent<PlayerMove>().enabled = false;
            showPaused();
        }
        else if (Time.timeScale == 0)
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
            player.GetComponent<PlayerMove>().enabled = true;
            hidePaused();
        }
    }

    //shows objects with ShowOnPause tag
    public void showPaused()
    {
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(true);
        }
    }

    //hides objects with ShowOnPause tag
    public void hidePaused()
    {
        HideAll();
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
        }
    }

    public void QuitGameButton()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public void HideAll()
    {
        quitValidation.SetActive(false);
        hintsPanel.SetActive(false);
    }

    public void ShowValidation()
    {
        quitValidation.SetActive(true);
    }

    public void HideValidation()
    {
        quitValidation.SetActive(false);
    }

    public void ShowHints()
    {
        hintsPanel.SetActive(true);
    }

    public void HideHints()
    {
        hintsPanel.SetActive(false);
    }

    public void MainMenuButton()
    {
        SceneManager.LoadScene("MenuTest");
    }
}
